function forAllPixels(height, width, job) {
  for (y = 0; y < height; y++) {
    for (x = 0; x < width; x++) {
      var index = y * width + x;
      job(x, y, index);
    }
  }
}

function clickOnPixel(event) {
  var pixel = event.srcElement;
  if (!pixel.value) {
    pixel.style.backgroundColor = "black";
  } else {
    pixel.style.backgroundColor = "white";
  }
  pixel.value = ! pixel.value
  updateResutls();
};

function removeCurrentField() {
  var canvas = document.querySelector(".canvas");
  canvas.innerHTML = '';
}

function createNewField() {
  var width = document.querySelector("#x").value
  var height = document.querySelector("#y").value
  var createButton = document.querySelector("button#create");
  createButton.textContent = "Update";
  var canvas = document.querySelector(".canvas");
  pixels = [];
  for (y = 0; y < height; y++) {
    var row = document.createElement("div")
    row.setAttribute("class", "row");
    for (x = 0; x < width; x++) {
      var pixel = document.createElement("span");
      pixel.setAttribute("class", "pixel");
      pixel.style.backgroundColor = "white";
      if (x == width - 1) {
        pixel.style.borderRight = "solid 1px #ccc";
      }
      if (y == height -1) {
        pixel.style.borderBottom = "solid 1px #ccc";
      }
      pixel.addEventListener("click", clickOnPixel);
      pixels.push(pixel);
      row.append(pixel);
    }
    canvas.append(row);
  }
  updateResutls();
}

function clickOnCreate(event) {
  removeCurrentField()
  createNewField()
}

function clickOnClear(event) {
  var width = document.querySelector("#x").value
  var height = document.querySelector("#y").value
  forAllPixels(height, width, function(x, y, index) {
    pixels[index].value = false;
    pixels[index].style.backgroundColor = "white";
  });
  updateResutls();
}

function updatePreview() {
  if (typeof pixels == 'undefined')
    return;
  var width = document.querySelector("#x").value
  var height = document.querySelector("#y").value
  var canvas = document.querySelector("#preview");
  var context = canvas.getContext("2d");
  context.canvas.width = width;
  context.canvas.height = height;
  image = context.createImageData(width, height);
  forAllPixels(height, width, function(x, y, index) {
    var pixelIndex = index * 4;
    var value = pixels[index].value;
    var color = 255;
    if (value) {
      color = 0;
    }
    image.data[pixelIndex] = color;
    image.data[pixelIndex + 1] = color;
    image.data[pixelIndex + 2] = color;
    image.data[pixelIndex + 3] = 255;
  });
  context.putImageData(image, 0, 0);
}

function updateGeneratedBytes() {
  if (typeof pixels === 'undefined')
    return;
  var width = document.querySelector("#x").value
  var height = document.querySelector("#y").value
  var bytes = [];
  forAllPixels(height, width, function(x, y, index) {
    var bytesPage = Math.floor(y / 8);
    var bytesIndex = x;
    if (typeof bytes[bytesPage] === 'undefined') {
      bytes[bytesPage] = [];
    }
    var value = pixels[index].value
    if (value) {
      var currVal = bytes[bytesPage][bytesIndex];
      if (typeof currVal === "undefined") {
        currVal = 0;
        bytes[bytesPage].push(currVal);
      }
      currVal += (1 << (y % 8));
      bytes[bytesPage][bytesIndex] = currVal;
    }
  });
  // Print result
  var pages = Math.floor(height / 8);
  var bytesInPage = width;
  var text = "";
  for (page = 0; page < pages; page++) {
    if (text.length > 0)
      text += ",\n";
    text += "{";
    for (b = 0; b < bytesInPage; b++) {
      var val = bytes[page][b];
      if (text.substr(text.length - 1) !== "{")
        text += ", ";
      if (typeof val === "undefined") {
        text += "0x00";
      } else {
        text += "0x"+val.toString(16).padStart(2, '0');
      }
    }
    text += "}";
  }
  var code = document.querySelector(".code");
  code.textContent = text;
}

function clickOnCreateFromFile() {
  var width = document.querySelector("#x").value
  var height = document.querySelector("#y").value
  var fileUrl = document.querySelector("input#file");
  var fReader = new FileReader();
  fReader.readAsDataURL(fileUrl.files[0]);
  fReader.onload = function(event) {
    var img = new Image();
    img.src = event.target.result;
    img.onload = function(event) {
      if (width != img.width || height != img.height) {
        alert("Invalid size of image.\nWanted: "+width+"x"+height+"\nLoaded: "+img.width+"x"+img.height);
        return;
      }
      var canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;
      var context = canvas.getContext("2d");
      context.drawImage(img, 0, 0);
      imgData = context.getImageData(0, 0, width, height);
      forAllPixels(height, width, function(x, y, index) {
        var imgIndex = index * 4;
        var pixel = pixels[index]
        if (imgData.data[imgIndex] !== 255 && imgData.data[imgIndex + 3] !== 0) {
          pixel.value = true;
          pixel.style.backgroundColor = "black";
        } else {
          pixel.value = false;
          pixel.style.backgroundColor = "white";
        }
      });
      updateResutls();
    }
  }
}

function updateResutls() {
  updatePreview();
  updateGeneratedBytes();
}

function init() {
  create = document.querySelector("button#create");
  create.addEventListener("click", clickOnCreate);
  clear = document.querySelector("button#clear");
  clear.addEventListener("click", clickOnClear);
  createFromFile = document.querySelector("button#createFromFile");
  createFromFile.addEventListener("click", clickOnCreateFromFile);
  updateResutls();
}

if( document.readyState !== 'loading' ) {
  init();
} else {
  document.addEventListener("DOMContentLoaded", function() {
    init();
  });
}
